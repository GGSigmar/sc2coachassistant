<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HeadControllerTest extends WebTestCase
{
    public function testCreateTeam()
    {
        $client = static::createClient();

        $container = self::$kernel->getContainer();
        $em = $container->get('doctrine')->getManager();
        $userRepo = $em->getRepository('UserBundle:User');
        /*$userRepo->createQueryBuilder('user')
            ->delete()
            ->getQuery()
            ->execute();*/

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'Sigmar',
            '_password'  => 'testowehaslo',
        ));

        $client->submit($form);

        $crawler = $client->followRedirect();

        $crawler = $client->request('GET', '/create-team');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Create team', $client->getResponse()->getContent());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertRegExp('/You already got a team/', $client->getResponse()->getContent());

        $crawler = $client->request('GET', '/logout');
        $crawler = $client->followRedirect();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form(array(
            '_username'  => 'John',
            '_password'  => 'johndoe',
        ));

        $client->submit($form);

        $crawler = $client->request('GET', '/create-team');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Create team', $client->getResponse()->getContent());

        $form = $crawler->selectButton('Create team')->form();
        $crawler = $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Your team needs a join password, sir!', $client->getResponse()->getContent());

        /* w tym miejscu powinienem jeszcze przetestowac czy prawidlowy form przejdzie, ale bitch please, jasne, ze tak
        bo piszę quality code... #rakMłodegoProgramisty - około 1:30 w tutorialu nr 2 */
    } /* test jest chwilowo zepsuty ze wzgledu na nowa baze testową, która nie ma potrzebnych rzeczy :) */
}
