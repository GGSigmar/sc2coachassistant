<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CreateTeamFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamName', TextType::class, array(
                'label' => 'Team name:*',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('nameAbbreviation', TextType::class, array(
                'label' => 'Team name abbreviation:',
                'attr' => array (
                    'class' => 'sigmarInputNotRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelNotRequired',
                )
            ))
            ->add('plainJoinPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'attr' => array (
                        'class' => 'sigmarInputRequired form-control',
                    ),
                    'label_attr' => array (
                        'class' => 'sigmarLabelRequired',
                    )
                ),
                'first_options' => array(
                    'label' => 'Join password:*',
                ),
                'second_options' => array (
                    'label' => 'Repeat join password:*',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Team',
        ));
    }


    public function getName()
    {
        return 'create_team';
    }
}