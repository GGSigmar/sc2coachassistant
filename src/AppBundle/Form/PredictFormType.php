<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PredictFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('map', EntityType::class, array(
                'class' => 'AppBundle\Entity\Map',
                'label' => 'Map:',
                'choice_label' => 'mapName',
                'choices' => $options['maps'],
                'placeholder' => 'Unknown',
                'empty_data' => null,
                'required' => false,
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('opponentRace', ChoiceType::class, array(
                'label' => 'Opponent\'s race:',
                'choices' => array(
                    'Terran' => 'Terran',
                    'Zerg' => 'Zerg',
                    'Protoss' => 'Protoss',
                ),
                'placeholder' => 'Unknown',
                'empty_data' => null,
                'required' => false,
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'maps' => [],
        ));
    }

    public function getName()
    {
        return 'predict';
    }
}
