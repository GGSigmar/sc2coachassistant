<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddGameFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('yourPlayer', EntityType::class, array(
                'class' => 'AppBundle\Entity\Player',
                'label' => 'Your player:',
                'choice_label' => 'nickname',
                'choices' => $options['players'],
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('map', EntityType::class, array(
                'class' => 'AppBundle\Entity\Map',
                'label' => 'Map:',
                'choice_label' => 'mapName',
                'choices' => $options['maps'],
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('date', DateType::class, array(
                'label' => 'Game\'s date:',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('opponentRace', ChoiceType::class, array(
                'label' => 'Opponent\'s race: ',
                'choices' => array(
                    'Terran' => 'Terran',
                    'Zerg' => 'Zerg',
                    'Protoss' => 'Protoss'
                ),
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('result', ChoiceType::class, array(
                'label' => 'Result:',
                'choices' => array(
                    'Victory' => 'win',
                    'Defeat' => 'lose',
                ),
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Game',
            'players' => [],
            'maps' => [],
        ));
    }

    public function getName()
    {
        return 'add_game';
    }
}
