<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class JoinTeamFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamNameOrAbb', TextType::class, array(
                'label' => 'Team name/abbreviation:*',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('plainJoinPassword', PasswordType::class, array(
                'label' => 'Team join password:*',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ));
    }

    public function getName()
    {
        return 'join_team';
    }
}