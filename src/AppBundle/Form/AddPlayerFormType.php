<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AddPlayerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickname', TextType::class, array(
                'label' => 'Player\'s nickname:',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ))
            ->add('race', ChoiceType::class, array(
                'label' => 'Player\'s race:',
                'choices' => array(
                    'Terran' => 'Terran',
                    'Zerg' => 'Zerg',
                    'Protoss' => 'Protoss'
                ),
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Player',
        ));
    }

    public function getName()
    {
        return 'add_player';
    }
}