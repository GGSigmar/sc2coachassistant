<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LeaveTeamFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('confirm', TextType::class, array(
                'label' => 'Confirmation:',
                'attr' => array (
                    'class' => 'sigmarInputRequired form-control',
                ),
                'label_attr' => array (
                    'class' => 'sigmarLabelRequired',
                )
            ));
    }

    public function getName()
    {
        return 'join_team';
    }
}