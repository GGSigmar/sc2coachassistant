<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PlayerRepository extends EntityRepository
{
    public function findAllPlayersByTeam($team)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.team = :team')
            ->setParameter('team', $team)
            ->orderBy('p.nickname', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findPlayerBySlug($slug)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPlayersByRace($race)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.race = :race')
            ->setParameter('race', $race)
            ->getQuery()
            ->getResult();
    }

    public function findOneByRace($race)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.race = :race')
            ->setParameter('race', $race)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPlayersOutsideTeam($team) {
        return $this->createQueryBuilder('p')
            ->andWhere('p.team != :team')
            ->setParameter('team', $team)
            ->orderBy('p.nickname', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByTeamAndRace($race, $team) {
        return $this->createQueryBuilder('p')
            ->andWhere('p.race = :race')
            ->andWhere('p.team = :team')
            ->setParameter('race', $race)
            ->setParameter('team', $team)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
