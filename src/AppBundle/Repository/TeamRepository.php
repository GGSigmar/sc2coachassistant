<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TeamRepository extends EntityRepository
{
    public function findOneByNameOrAbbreviation($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.teamName = :value OR t.nameAbbreviation = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
