<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GameRepository extends EntityRepository
{
    public function findAllGamesByPlayers($players)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->setParameter('players', $players)
            ->orderBy('g.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonGamesByPlayers($players)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.result = :win')
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->orderBy('g.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findRecentGamesByPlayers($players)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->setParameter('players', $players)
            ->setMaxResults(20)
            ->orderBy('g.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonGamesVsRaceByPlayers($players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace = :race')
            ->andWhere('g.result = :win')
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllGamesVsRaceByPlayers($players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace = :race')
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findGameById($id)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllGamesByMap($map)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->setParameter('map', $map)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllGamesByPlayersOnMap($map, $players)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->setParameter('map', $map)
            ->setParameter('players', $players)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonGamesByPlayersOnMap($map, $players)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.result = :win')
            ->setParameter('map', $map)
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonRacePlayers($players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace != :race')
            ->andWhere('g.result = :win')
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllRacePlayers($players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace != :race')
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonRacePlayersGamesOnMap($map, $players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace != :race')
            ->andWhere('g.result = :win')
            ->setParameter('map', $map)
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllRacePlayersGamesOnMap($map, $players, $race)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace != :race')
            ->setParameter('map', $map)
            ->setParameter('players', $players)
            ->setParameter('race', $race)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findWonPlayersVsRaceGamesOnMap($map, $players, $opponentRace)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace = :opponentRace')
            ->andWhere('g.result = :win')
            ->setParameter('map', $map)
            ->setParameter('win', "win")
            ->setParameter('players', $players)
            ->setParameter('opponentRace', $opponentRace)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllPlayersVsRaceGamesOnMap($map, $players, $opponentRace)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.map = :map')
            ->andWhere('g.yourPlayer IN (:players)')
            ->andWhere('g.opponentRace = :opponentRace')
            ->setParameter('map', $map)
            ->setParameter('players', $players)
            ->setParameter('opponentRace', $opponentRace)
            ->orderBy('g.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
