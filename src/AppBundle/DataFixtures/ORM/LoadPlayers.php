<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Player;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPlayers implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $SKTelecomT1 = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'SK Telecom T1'
            ));

        $AfreecaFreecs = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'Afreeca Freecs'
            ));

        $AnonymousTeam = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'Anonymous Team'
            ));

        // anonymous team

        $AnonymousTerran = new Player();
        $AnonymousTerran
            ->setNickname("Terran")
            ->setRace("Terran")
            ->setTeam($AnonymousTeam);

        $AnonymousZerg = new Player();
        $AnonymousZerg
            ->setNickname("Zerg")
            ->setRace("Zerg")
            ->setTeam($AnonymousTeam);

        $AnonymousProtoss = new Player();
        $AnonymousProtoss
            ->setNickname("Protoss")
            ->setRace("Protoss")
            ->setTeam($AnonymousTeam);

        $manager->persist($AnonymousTerran);
        $manager->persist($AnonymousZerg);
        $manager->persist($AnonymousProtoss);

        // sk telecom t1

        $Classic = new Player();
        $Classic
            ->setNickname("Classic")
            ->setRace("Protoss")
            ->setTeam($SKTelecomT1);

        $Zoun = new Player();
        $Zoun
            ->setNickname("Zoun")
            ->setRace("Protoss")
            ->setTeam($SKTelecomT1);

        $INnoVation = new Player();
        $INnoVation
            ->setNickname("INnoVation")
            ->setRace("Terran")
            ->setTeam($SKTelecomT1);

        $Dark = new Player();
        $Dark
            ->setNickname("Dark")
            ->setRace("Zerg")
            ->setTeam($SKTelecomT1);

        $soO = new Player();
        $soO
            ->setNickname("soO")
            ->setRace("Zerg")
            ->setTeam($SKTelecomT1);

        $Dream = new Player();
        $Dream
            ->setNickname("Dream")
            ->setRace("Terran")
            ->setTeam($SKTelecomT1);

        $manager->persist($Classic);
        $manager->persist($Zoun);
        $manager->persist($INnoVation);
        $manager->persist($Dark);
        $manager->persist($soO);
        $manager->persist($Dream);

        // afreeca freecs
        
        $Curious = new Player();
        $Curious
            ->setNickname("Curious")
            ->setRace("Zerg")
            ->setTeam($AfreecaFreecs);

        $Super = new Player();
        $Super
            ->setNickname("Super")
            ->setRace("Protoss")
            ->setTeam($AfreecaFreecs);

        $Patience = new Player();
        $Patience
            ->setNickname("Patience")
            ->setRace("Protoss")
            ->setTeam($AfreecaFreecs);

        $aLive = new Player();
        $aLive
            ->setNickname("aLive")
            ->setRace("Terran")
            ->setTeam($AfreecaFreecs);

        $Symbol = new Player();
        $Symbol
            ->setNickname("Symbol")
            ->setRace("Zerg")
            ->setTeam($AfreecaFreecs);

        $Bomber = new Player();
        $Bomber
            ->setNickname("Bomber")
            ->setRace("Terran")
            ->setTeam($AfreecaFreecs);

        $manager->persist($Curious);
        $manager->persist($Super);
        $manager->persist($Patience);
        $manager->persist($aLive);
        $manager->persist($Symbol);
        $manager->persist($Bomber);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 40;
    }
}