<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Team;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsers implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $SKTelecomT1 = new Team();
        $SKTelecomT1->setTeamName("SK Telecom T1");
        $SKTelecomT1->setNameAbbreviation("SKT");
        $SKTelecomT1->setPlainJoinPassword("skt");

        $AfreecaFreecs = new Team();
        $AfreecaFreecs->setTeamName("Afreeca Freecs");
        $AfreecaFreecs->setNameAbbreviation("AF");
        $AfreecaFreecs->setPlainJoinPassword("af");

        $AnonymousTeam = new Team();
        $AnonymousTeam->setTeamName("Anonymous Team");
        $AnonymousTeam->setNameAbbreviation("AT-AT");
        $AnonymousTeam->setPlainJoinPassword("anonymous");
        
        $manager->persist($SKTelecomT1);
        $manager->persist($AfreecaFreecs);
        $manager->persist($AnonymousTeam);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 10;
    }
}