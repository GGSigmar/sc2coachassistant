<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAFGames implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //team

        $AfreecaFreecs = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'Afreeca Freecs'
            ));

        //players

        $Curious = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Curious',
                'team' => $AfreecaFreecs
            ));

        $Super = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Super',
                'team' => $AfreecaFreecs
            ));

        $Patience = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Patience',
                'team' => $AfreecaFreecs
            ));

        $aLive = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'aLive',
                'team' => $AfreecaFreecs
            ));

        $Symbol = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Symbol',
                'team' => $AfreecaFreecs
            ));

        $Bomber = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Bomber',
                'team' => $AfreecaFreecs
            ));

        //maps

        $duskTowers = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Dusk Towers'
            ));

        $frozenTemple = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Frozen Temple'
            ));

        $frost = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Frost'
            ));

        $kingSejongStation = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'King Sejong Station'
            ));

        $newGettysburg = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'New Gettysburg'
            ));

        $overgrowth = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Overgrowth'
            ));

        //games

        $SKTvsAF1 = new Game();
        $SKTvsAF1
            ->setMap($overgrowth)
            ->setYourPlayer($Curious)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF2 = new Game();
        $SKTvsAF2
            ->setMap($duskTowers)
            ->setYourPlayer($Super)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF3 = new Game();
        $SKTvsAF3
            ->setMap($kingSejongStation)
            ->setYourPlayer($Patience)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF4 = new Game();
        $SKTvsAF4
            ->setMap($frozenTemple)
            ->setYourPlayer($aLive)
            ->setOpponentRace("Zerg")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF5 = new Game();
        $SKTvsAF5
            ->setMap($newGettysburg)
            ->setYourPlayer($Symbol)
            ->setOpponentRace("Zerg")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF6 = new Game();
        $SKTvsAF6
            ->setMap($frost)
            ->setYourPlayer($Bomber)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF7 = new Game();
        $SKTvsAF7
            ->setMap($frozenTemple)
            ->setYourPlayer($Patience)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF8 = new Game();
        $SKTvsAF8
            ->setMap($kingSejongStation)
            ->setYourPlayer($Super)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF9 = new Game();
        $SKTvsAF9
            ->setMap($frost)
            ->setYourPlayer($Super)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF10 = new Game();
        $SKTvsAF10
            ->setMap($duskTowers)
            ->setYourPlayer($aLive)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF11 = new Game();
        $SKTvsAF11
            ->setMap($overgrowth)
            ->setYourPlayer($Curious)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $manager->persist($SKTvsAF1);
        $manager->persist($SKTvsAF2);
        $manager->persist($SKTvsAF3);
        $manager->persist($SKTvsAF4);
        $manager->persist($SKTvsAF5);
        $manager->persist($SKTvsAF6);
        $manager->persist($SKTvsAF7);
        $manager->persist($SKTvsAF8);
        $manager->persist($SKTvsAF9);
        $manager->persist($SKTvsAF10);
        $manager->persist($SKTvsAF11);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 52;
    }
}
