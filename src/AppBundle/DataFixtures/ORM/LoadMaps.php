<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Map;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadMaps implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $duskTowers = new Map();
        $duskTowers->setMapName("Dusk Towers");

        $frozenTemple = new Map();
        $frozenTemple->setMapName("Frozen Temple");

        $frost = new Map();
        $frost->setMapName("Frost");

        $kingSejongStation = new Map();
        $kingSejongStation->setMapName("King Sejong Station");

        $newGettysburg = new Map();
        $newGettysburg->setMapName("New Gettysburg");

        $overgrowth = new Map();
        $overgrowth->setMapName("Overgrowth");

        $manager->persist($duskTowers);
        $manager->persist($frozenTemple);
        $manager->persist($frost);
        $manager->persist($kingSejongStation);
        $manager->persist($newGettysburg);
        $manager->persist($overgrowth);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 30;
    }
}