<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadGames implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //team

        $SKTelecomT1 = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'SK Telecom T1'
            ));

        //players

        $Classic = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Classic',
                'team' => $SKTelecomT1
            ));

        $Zoun = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Zoun',
                'team' => $SKTelecomT1
            ));

        $INnoVation = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'INnoVation',
                'team' => $SKTelecomT1
            ));

        $Dark = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Dark',
                'team' => $SKTelecomT1
            ));

        $soO = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'soO',
                'team' => $SKTelecomT1
            ));

        $Dream = $manager->getRepository('AppBundle:Player')
            ->findOneBy(array (
                'nickname' => 'Dream',
                'team' => $SKTelecomT1
            ));

        //maps

        $duskTowers = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Dusk Towers'
            ));

        $frozenTemple = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Frozen Temple'
            ));

        $frost = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Frost'
            ));

        $kingSejongStation = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'King Sejong Station'
            ));

        $newGettysburg = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'New Gettysburg'
            ));

        $overgrowth = $manager->getRepository('AppBundle:Map')
            ->findOneBy(array (
                'mapName' => 'Overgrowth'
            ));

        //games

        $SKTvsAF1 = new Game();
        $SKTvsAF1
            ->setMap($overgrowth)
            ->setYourPlayer($Classic)
            ->setOpponentRace("Zerg")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF2 = new Game();
        $SKTvsAF2
            ->setMap($duskTowers)
            ->setYourPlayer($Zoun)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF3 = new Game();
        $SKTvsAF3
            ->setMap($kingSejongStation)
            ->setYourPlayer($INnoVation)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF4 = new Game();
        $SKTvsAF4
            ->setMap($frozenTemple)
            ->setYourPlayer($Dark)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF5 = new Game();
        $SKTvsAF5
            ->setMap($newGettysburg)
            ->setYourPlayer($soO)
            ->setOpponentRace("Zerg")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF6 = new Game();
        $SKTvsAF6
            ->setMap($frost)
            ->setYourPlayer($Dream)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF7 = new Game();
        $SKTvsAF7
            ->setMap($frozenTemple)
            ->setYourPlayer($INnoVation)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF8 = new Game();
        $SKTvsAF8
            ->setMap($kingSejongStation)
            ->setYourPlayer($INnoVation)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("lose");

        $SKTvsAF9 = new Game();
        $SKTvsAF9
            ->setMap($frost)
            ->setYourPlayer($Classic)
            ->setOpponentRace("Protoss")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF10 = new Game();
        $SKTvsAF10
            ->setMap($duskTowers)
            ->setYourPlayer($Classic)
            ->setOpponentRace("Terran")
            ->setDate(new \DateTime())
            ->setResult("win");

        $SKTvsAF11 = new Game();
        $SKTvsAF11
            ->setMap($overgrowth)
            ->setYourPlayer($Classic)
            ->setOpponentRace("Zerg")
            ->setDate(new \DateTime())
            ->setResult("win");

        $manager->persist($SKTvsAF1);
        $manager->persist($SKTvsAF2);
        $manager->persist($SKTvsAF3);
        $manager->persist($SKTvsAF4);
        $manager->persist($SKTvsAF5);
        $manager->persist($SKTvsAF6);
        $manager->persist($SKTvsAF7);
        $manager->persist($SKTvsAF8);
        $manager->persist($SKTvsAF9);
        $manager->persist($SKTvsAF10);
        $manager->persist($SKTvsAF11);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 51;
    }
}
