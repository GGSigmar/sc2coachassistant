<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Player
 *
 * @ORM\Table(name="sc2ca_player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Your player needs a nickname, sir!")
     * @Assert\Length(min=3, minMessage="Your player's nickname needs to be at least 3 characters long, sir!")
     * @Assert\Length(max=24, maxMessage="Your player's nickname cannot be longer than 24 characters, sir!")
     * @ORM\Column(name="nickname", type="string", length=255)
     */
    private $nickname;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Your player needs a nickname, sir!")
     * @Assert\Choice({"Terran", "Zerg", "Protoss"}, message="Player's race can only be chosen between Terran, Zerg and Protoss, sir!")
     * @ORM\Column(name="race", type="string", length=255)
     */
    private $race;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Team",
     *     inversedBy="players"
     * )
     */
    private $team;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Game", mappedBy="yourPlayer")
     */
    private $games;

    /**
     * @ORM\Column(name="slug", unique=true)
     * @Gedmo\Slug(fields={"nickname", "race"}, updatable=false)
     */
    private $slug;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return Player
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set race
     *
     * @param string $race
     *
     * @return Player
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Sets Team to null
     */
    public function unsetTeam()
    {
        $this->team = null;
    }
}

