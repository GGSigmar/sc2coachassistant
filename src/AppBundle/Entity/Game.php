<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Game
 *
 * @ORM\Table(name="sc2ca_game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Map",
     *     inversedBy="games"
     * )
     */
    private $map;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Player",
     *     inversedBy="games"
     * )
     */
    private $yourPlayer;

    /**
     * @var string
     *
     * @Assert\Choice({"Terran", "Zerg", "Protoss"}, message="You need to provide opponent's race, sir!")
     * @ORM\Column(name="opponentRace", type="string", length=255)
     */
    private $opponentRace;

    /**
     * @var string
     *
     * @Assert\Choice({"win", "lose"}, message="The result can only be a win or a lose, sir!")
     * @ORM\Column(name="result", type="string", length=255)
     */
    private $result;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set map
     *
     * @param Map $map
     *
     * @return Game
     */
    public function setMap(Map $map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Game
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return Player
     */
    public function getYourPlayer()
    {
        return $this->yourPlayer;
    }

    /**
     * @param Player $yourPlayer
     */
    public function setYourPlayer(Player $yourPlayer)
    {
        $this->yourPlayer = $yourPlayer;

        return $this;
    }

    /**
     * @return string
     */
    public function getOpponentRace()
    {
        return $this->opponentRace;
    }

    /**
     * @param string $opponentRace
     */
    public function setOpponentRace($opponentRace)
    {
        $this->opponentRace = $opponentRace;

        return $this;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }
}

