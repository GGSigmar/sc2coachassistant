<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Team
 *
 * @ORM\Table(name="sc2ca_team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 * @UniqueEntity(fields="teamName", message="Your team's name has to be unique and there is already a team with provided name, sir!")
 * @UniqueEntity(fields="nameAbbreviation", message="Your team's name abbreviation has to be unique and there is already a team with provided abbreviation, sir!")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Your team needs a name, sir!")
     * @Assert\Length(min=4, minMessage="Your team's name needs to be at least 4 characters long, sir!")
     * @Assert\Length(max=32, maxMessage="Your team's name cannot be longer than 32 characters, sir!")
     * @ORM\Column(name="teamName", type="string", length=255, unique=true)
     */
    private $teamName;

    /**
     * @var string
     *
     * @Assert\Length(min=2, minMessage="Your team's name abbreviation needs to be at least 2 characters long, sir!")
     * @Assert\Length(max=32, maxMessage="Your team's name abbreviation cannot be longer than 6 characters, sir!")
     * @ORM\Column(name="nameAbbreviation", type="string", length=255, nullable=true, unique=true)
     */
    private $nameAbbreviation;

    /**
     * @var string
     *
     * @Orm\Column(name="joinPassword", type="string", length=255, nullable=true)
     */
    private $joinPassword;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Your team needs a join password, sir!")
     * @Assert\Length(min=8, minMessage="Your team's join password needs to be at least 8 characters long, sir!")
     * @Assert\Length(max=32, maxMessage="Your team's join password cannot be longer than 32 characters, sir!")
     * @Orm\Column(name="plainJoinPassword", type="string", length=255, nullable=true)
     */
    private $plainJoinPassword;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\User", mappedBy="team")
     */
    private $managers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Player", mappedBy="team")
     */
    private $players;

    /**
     * @ORM\Column(name="slug", unique=true)
     * @Gedmo\Slug(fields={"teamName"}, updatable=false)
     */
    private $slug;

    public function __construct()
    {
        $this->managers = new ArrayCollection();
        $this->players = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     *
     * @return Team
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * Set nameAbbreviation
     *
     * @param string $nameAbbreviation
     *
     * @return Team
     */
    public function setNameAbbreviation($nameAbbreviation)
    {
        $this->nameAbbreviation = $nameAbbreviation;

        return $this;
    }

    /**
     * Get nameAbbreviation
     *
     * @return string
     */
    public function getNameAbbreviation()
    {
        return $this->nameAbbreviation;
    }

    /**
     * @return string
     */
    public function getJoinPassword()
    {
        return $this->joinPassword;
    }

    /**
     * @param string $joinPassword
     */
    public function setJoinPassword($joinPassword)
    {
        $this->joinPassword = $joinPassword;
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPlainJoinPassword()
    {
        return $this->plainJoinPassword;
    }

    /**
     * @param string $plainJoinPassword
     */
    public function setPlainJoinPassword($plainJoinPassword)
    {
        $this->plainJoinPassword = $plainJoinPassword;
    }

    public function eraseCredentials()
    {
        $this->setPlainJoinPassword(null);
    }

    public function __toString()
    {
        return $this->teamName;
    }

    /**
     * @return mixed
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @return mixed
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }
}

