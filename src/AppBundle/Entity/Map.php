<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Map
 *
 * @ORM\Table(name="sc2ca_map")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MapRepository")
 * @UniqueEntity(fields="mapName", message="Map's name has to be unique and there is already a map with provided name, sir!")
 */
class Map
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="You need to provide a map name, sir!")
     * @Assert\Length(min=3, minMessage="Map's name needs to be at least 3 characters long, sir!")
     * @Assert\Length(max=24, maxMessage="Map's name cannot be longer than 32 characters, sir!")
     * @ORM\Column(name="mapName", type="string", length=255, unique=true)
     */
    private $mapName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Game", mappedBy="map")
     */
    private $games;

    /**
     * @ORM\Column(name="slug", unique=true)
     * @Gedmo\Slug(fields={"mapName"}, updatable=false)
     */
    private $slug;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mapName
     *
     * @param string $mapName
     *
     * @return Map
     */
    public function setMapName($mapName)
    {
        $this->mapName = $mapName;

        return $this;
    }

    /**
     * Get mapName
     *
     * @return string
     */
    public function getMapName()
    {
        return $this->mapName;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function __toString()
    {
        return $this->mapName;
    }
}

