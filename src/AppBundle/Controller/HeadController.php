<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Entity\Map;
use AppBundle\Entity\Player;
use AppBundle\Entity\Team;
use AppBundle\Form\CreateTeamFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HeadController extends SecurityController
{
    /**
     * @Route("/", name="index")
     * @Template("Basic/index.html.twig")
     */
    public function indexAction()
    {
        if($this->userHasTeam()) {
            $team = $this->getUserTeam();

            $url = $this->generateUrl('show_team', array('slug' => $team->getSlug()));
            return $this->redirect($url);
        } else {
            return null;
        }
    }

    /**
     * @Route("/populate/{times}", name="populate")
     * @Template("Basic/populate.html.twig")
     */
    public function populateGamesAction($times, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        $times = intval($times);

        $em = $this->getDoctrine()->getManager();
        $maps = $em->getRepository('AppBundle:Map')
            ->findAll();
        $races = array("Terran", "Zerg", "Protoss");
        $results = array("win", "lose");
        $date = new \DateTime();

        $AnonymousTeam = $em->getRepository('AppBundle:Team')
            ->findOneBy(array(
                'teamName' => 'Anonymous Team'
            ));

        $nonAnonymousPlayers = $em->getRepository('AppBundle:Player')
            ->findAllPlayersOutsideTeam($AnonymousTeam);

        for($i = 0; $i < $times; $i++)
        {
            $randomMap = $maps[array_rand($maps, 1)];
            $randomPlayer = $nonAnonymousPlayers[array_rand($nonAnonymousPlayers, 1)];
            $randomOpponentRace = $races[array_rand($races, 1)];
            $randomResult = $results[array_rand($results, 1)];

            $game = new Game();
            $game->setMap($randomMap);
            $game->setYourPlayer($randomPlayer);
            $game->setOpponentRace($randomOpponentRace);
            $game->setDate($date);
            $game->setResult($randomResult);

            $playerRace = $randomPlayer->getRace();

            $playerOpposite = $em->getRepository('AppBundle:Player')
                ->findOneByTeamAndRace($game->getOpponentRace(), $AnonymousTeam);

            $gameOpposite = new Game();
            $gameOpposite->setMap($game->getMap());
            $gameOpposite->setYourPlayer($playerOpposite);
            $gameOpposite->setOpponentRace($playerRace);
            $gameOpposite->setDate($game->getDate());

            if($game->getResult() == "win") {
                $gameOpposite->setResult("lose");
            } else {
                $gameOpposite->setResult("win");
            }

            $em->persist($game);
            $em->persist($gameOpposite);
            $em->flush();
        }

        $times = $times*2;

        $this->addFlash('success', 'A lot of game data just appeared out of nowhere!');
        return $this->redirectToIndex();
    }

    /**
     * @Route("/team/{slug}", name="show_team")
     * @Template("Basic/showTeam.html.twig")
     */
    public function showTeamAction($slug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($slug)) {
            $team = $this->getUserTeam();

            $em = $this->getDoctrine()->getManager();

            $gameRepo = $em->getRepository('AppBundle:Game');
            $playerRepo = $em->getRepository('AppBundle:Player');

            $allPlayers = $playerRepo->findAllPlayersByTeam($team);
            $recentGames = $gameRepo->findRecentGamesByPlayers($allPlayers);
            $allGames = $gameRepo->findAllGamesByPlayers($allPlayers);

            $playerWinrates = array();

            foreach($allPlayers as $player) {
                $allOverall = count($gameRepo->findAllGamesByPlayers($player));
                $wonOverall = count($gameRepo->findWonGamesByPlayers($player));
                if($allOverall == 0) {
                    $percentOverall = '-';
                } else {
                    $percentOverall = round(($wonOverall/$allOverall), 2);
                    $percentOverall = ($percentOverall * 100) . '%';
                }

                $wonTerran = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Terran"));
                $allTerran = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Terran"));
                if($allTerran == 0) {
                    $percentTerran = '-';
                } else {
                    $percentTerran = round(($wonTerran/$allTerran), 2);
                    $percentTerran = ($percentTerran * 100) . '%';
                }

                $wonZerg = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Zerg"));
                $allZerg = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Zerg"));
                if($allZerg == 0) {
                    $percentZerg = '-';
                } else {
                    $percentZerg = round(($wonZerg/$allZerg), 2);
                    $percentZerg = ($percentZerg * 100) . '%';
                }

                $wonProtoss = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Protoss"));
                $allProtoss = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Protoss"));
                if($allProtoss == 0) {
                    $percentProtoss = '-';
                } else {
                    $percentProtoss = round(($wonProtoss/$allProtoss), 2);
                    $percentProtoss = ($percentProtoss * 100) . '%';
                }

                $playerWinrates[$player->getNickname()]['Slug'] = $player->getSlug();
                $playerWinrates[$player->getNickname()]['Nickname'] = $player->getNickname();
                $playerWinrates[$player->getNickname()]['Overall'] = $percentOverall;
                $playerWinrates[$player->getNickname()]['Terran'] = $percentTerran;
                $playerWinrates[$player->getNickname()]['Zerg'] = $percentZerg;
                $playerWinrates[$player->getNickname()]['Protoss'] = $percentProtoss;
            }

            return array(
                'team' => $team,
                'players' => $allPlayers,
                'allGames' => $allGames,
                'recentGames' => $recentGames,
                'playerWinrates' => $playerWinrates,
            );
        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/create-team", name="create_team")
     * @Template("Basic/createTeam.html.twig")
     */
    public function createTeamAction(Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($antiHasTeamSecurity = $this->enforceAntiUserHasTeam($request))) { return $antiHasTeamSecurity; }

        $defaultTeam = new Team();
        $defaultTeam->setTeamName("Team Default");
        $defaultTeam->setNameAbbreviation("DFLT");

        $form = $this->createForm('AppBundle\Form\CreateTeamFormType', $defaultTeam);

        $form->handleRequest($request);

        if($form->isValid()) {
            $team = $form->getData();

            /* moze jak rozkminie encodowanie hasla...) */
            //$team->setJoinPassword($this->encodePassword($team, $team->getPlainJoinPassword()));
            //$team->eraseCredentials();

            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();

            $user = $this->getUser();
            $user->setTeam($team);

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'You\'ve created your team, sir!');

            return $this->redirectToIndex();
        }

        return array ('form' => $form->createView());
    }

    /**
     * @Route("/join-team", name="join_team")
     * @Template("Basic/joinTeam.html.twig")
     */
    public function joinTeamAction(Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($antiHasTeamSecurity = $this->enforceAntiUserHasTeam($request))) { return $antiHasTeamSecurity; }

        $defaults = array (
            'teamName' => "Team Default"
        );

        $form = $this->createForm('AppBundle\Form\JoinTeamFormType', $defaults);

        $form->handleRequest($request);

        if($form->isValid()) {
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $team = $em->getRepository('AppBundle:Team')
                ->findOneByNameOrAbbreviation($data['teamNameOrAbb']);

            if(!is_null($team)) {
                if($team->getPlainJoinPassword() == $data['plainJoinPassword'])
                {
                    $this->getUser()->setTeam($team);
                    $em->persist($this->getUser());
                    $em->persist($team);
                    $em->flush();

                    $this->addFlash('success', 'You\'ve just joined the team, sir!');
                    return $this->redirectToIndex();
                } else {
                    $this->addFlash('warning', 'Team and its join password do not match, sir!');
                    $url = $this->generateUrl('join_team');
                    return $this->redirect($url);
                }
            } else {
                $this->addFlash('warning', 'There is no team with specified name or abbreviation, sir!');
                $url = $this->generateUrl('join_team');
                return $this->redirect($url);
            }
        }

        return array ('form' => $form->createView());
    }

    /**
     * @Route("/leave-team", name="leave_team")
     * @Template("Basic/leaveTeam.html.twig")
     */
    public function leaveTeamAction(Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        $form = $this->createForm('AppBundle\Form\LeaveTeamFormType');

        $form->handleRequest($request);

        if($form->isValid()) {
            $data = $form->getData();

            if($data['confirm'] == "CONFIRM") {
                $em = $this->getDoctrine()->getManager();
                $team = $this->getUser()->getTeam();

                $this->getUser()->unsetTeam();

                $em->persist($this->getUser());
                $em->persist($team);
                $em->flush();

                $this->addFlash('success', 'You\'ve just left your team, SHAME!');
                return $this->redirectToIndex();
            } else {
                $this->addFlash('warning', 'Confirmation failed, try again, if you still want to leave your team, sir!');
                $url = $this->generateUrl('leave_team');
                return $this->redirect($url);
            }
        }

        return array ('form' => $form->createView());
    }

    /**
     * @Route("/team/{tslug}/player/{pslug}", name="show_player")
     * @Template("Player/showPlayer.html.twig")
     */
    public function showPlayerAction($tslug, $pslug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($tslug)) {

            $em = $this->getDoctrine()->getManager();
            $player = $em->getRepository('AppBundle:Player')
                ->findPlayerBySlug($pslug);

            if($this->isCorrectPlayer($player)) {
                return $this->getPlayerStatistics($player);
            } else {
                $this->addFlash('warning', 'Don\'t you try to fiddle with other people\'s players, sir!');
                return $this->redirectToIndex();
            }
        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/team/{slug}/add-player", name="add_player")
     * @Template("Player/addPlayer.html.twig")
     */
    public function addPlayerAction($slug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($slug)) {
            $team = $this->getUserTeam();

            $defaultPlayer = new Player();
            $defaultPlayer->setNickname("DefaultPlayer");
            $defaultPlayer->setRace("Terran");
            $defaultPlayer->setTeam($team);

            $form = $this->createForm('AppBundle\Form\AddPlayerFormType', $defaultPlayer);
            $form->handleRequest($request);

            if($form->isValid()) {
                $player = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($player);
                $em->flush();

                $em->persist($team);
                $em->flush();

                $this->addFlash('success', 'You\'ve added your player, sir!');
                return $this->redirectToIndex();
            }

            return array (
                'form' => $form->createView(),
                'team' => $team,
                );

        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/team/{tslug}/remove-player/{pslug}", name="remove_player")
     * @Template("Player/removePlayer.html.twig")
     */
    public function removePlayerAction($tslug, $pslug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($tslug)) {

            $em = $this->getDoctrine()->getManager();
            $player = $em->getRepository('AppBundle:Player')
                ->findPlayerBySlug($pslug);

            if($this->isCorrectPlayer($player)) {
                $player->unsetTeam();
                $em->persist($player);
                $em->flush();
                $this->addFlash('success', 'You\'ve removed'.$player->getNickname().' from your team, sir!');
                return $this->redirectToIndex();
            } else {
                $this->addFlash('warning', 'Don\'t you try to fiddle with other people\'s players, sir!');
                return $this->redirectToIndex();
            }
        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/team/{slug}/game/{id}", name="show_game")
     * @Template("Game/showGame.html.twig")
     */
    public function showGameAction($slug, $id, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($slug)) {
            $em = $this->getDoctrine()->getManager();
            $game = $em->getRepository('AppBundle:Game')
                ->findGameById($id);

            if($this->isCorrectGame($game)) {
                return array(
                    'game' => $game,
                );
            } else {
                $this->addFlash('warning', 'Don\'t you try to fiddle with other people\'s games, sir!');
                return $this->redirectToIndex();
            }
        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/team/{slug}/add-game", name="add_game")
     * @Template("Game/addGame.html.twig")
     */
    public function addGameAction($slug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($slug)) {
            $team = $this->getUserTeam();

            $em = $this->getDoctrine()->getManager();
            $players = $em->getRepository('AppBundle:Player')
                ->findAllPlayersByTeam($team);

            $maps = $em->getRepository('AppBundle:Map')
                ->findAll();

            $form = $this->createForm('AppBundle\Form\AddGameFormType', new Game(), array(
                'players' => $players, 'maps' => $maps
            ));
            $form->handleRequest($request);

            if($form->isValid()) {
                $game = $form->getData();

                $AnonymousTeam = $em->getRepository('AppBundle:Team')
                    ->findOneBy(array(
                        'teamName' => 'Anonymous Team'
                    ));

                $player = $game->getYourPlayer();
                $playerRace = $player->getRace();

                $playerOpposite = $em->getRepository('AppBundle:Player')
                    ->findOneByTeamAndRace($game->getOpponentRace(), $AnonymousTeam);

                $gameOpposite = new Game();
                $gameOpposite->setMap($game->getMap());
                $gameOpposite->setYourPlayer($playerOpposite);
                $gameOpposite->setOpponentRace($playerRace);
                $gameOpposite->setDate($game->getDate());

                if($game->getResult() == "win") {
                    $gameOpposite->setResult("lose");
                } else {
                    $gameOpposite->setResult("win");
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($game);
                $em->persist($gameOpposite);
                $em->flush();

                $this->addFlash('success', 'You\'ve added your game, sir!');
                return $this->redirectToIndex();
            }

            return array (
                'form' => $form->createView(),
                'team' => $team,
            );

        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/team/{slug}/remove-game/{id}", name="remove_game")
     * @Template("Game/removeGame.html.twig")
     */
    public function removeGameAction($slug, $id, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        if($this->isCorrectTeam($slug)) {
            $em = $this->getDoctrine()->getManager();
            $game = $em->getRepository('AppBundle:Game')
                ->findGameById($id);

            if($this->isCorrectGame($game)) {
                $em->remove($game);
                $em->flush();
                $this->addFlash('success', 'You\'ve removed the game from the system, sir!');
                return $this->redirectToIndex();
            } else {
                $this->addFlash('warning', 'Don\'t you try to fiddle with other people\'s games, sir!');
                return $this->redirectToIndex();
            }
        } else {
            $this->addFlash('warning', 'Don\'t you try to access other people\'s teams, sir!');
            return $this->redirectToIndex();
        }
    }

    /**
     * @Route("/add-map", name="add_map")
     * @Template("Basic/addMap.html.twig")
     */
    public function addMapAction(Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        $defaultMap = new Map();
        $defaultMap->setMapName("Default Map Name");

        $form = $this->createForm('AppBundle\Form\AddMapFormType', $defaultMap);
        $form->handleRequest($request);

        if($form->isValid()) {
            $map = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($map);
            $em->flush();

            $this->addFlash('success', 'You\'ve added a map, sir!');
            return $this->redirectToIndex();
        }

        return array (
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/map/{slug}/", name="show_map")
     * @Template("Basic/showMap.html.twig")
     */
    public function showMapAction($slug, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        return $this->getMapStatistics($slug);
    }

    /**
     * @Route("/race/{race}/", name="show_race")
     * @Template("Basic/showRace.html.twig")
     */
    public function showRaceAction($race, Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        return $this->getRaceStatistics($race);
    }

    /**
     * @Route("/predict", name="predict")
     * @Template("Basic/predict.html.twig")
     */
    public function predictAction(Request $request)
    {
        if(!is_null($loggedInSecurity = $this->enforceUserLoggedIn($request))) { return $loggedInSecurity; }
        if(!is_null($hasTeamSecurity = $this->enforceUserHasTeam($request))) { return $hasTeamSecurity; }

        $team = $this->getUserTeam();

        $em = $this->getDoctrine()->getManager();
        $playerRepo = $em->getRepository('AppBundle:Player');
        $gameRepo = $em->getRepository('AppBundle:Game');
        $players = $playerRepo->findAllPlayersByTeam($team);

        $maps = $em->getRepository('AppBundle:Map')
            ->findAll();

        $form = $this->createForm('AppBundle\Form\PredictFormType', null, array(
            'maps' => $maps
        ));
        $form->handleRequest($request);
        if($form->isValid()) {
            $data = $form->getData();
            $chosenMap = $data['map'];
            $chosenRace = $data['opponentRace'];

            // actual predictions

            $winrates = array();

            if(is_null($chosenMap) && is_null($chosenRace)) {
                foreach($players as $player) {
                    $allOverall = count($gameRepo->findAllGamesByPlayers($player));
                    $wonOverall = count($gameRepo->findWonGamesByPlayers($player));

                    if($allOverall == 0) {
                        $overallWinrate = 45;
                    } else {
                        $overallWinrate = round(($wonOverall/$allOverall), 2) * 100;
                    }

                    $winrates[$player->getNickname()] = $overallWinrate;
                    $players[$player->getNickname()] = $player;
                }

                $winratesDescending = $winrates;
                arsort($winratesDescending);

                if(count($winratesDescending) > 5) {
                    $difference = count($winratesDescending) - 5;
                    for($i = 0; $i < $difference; $i++) {
                        array_pop($winratesDescending);
                    }
                }

                $chosenMap = 'Unknown';
                $chosenRace = 'Unknown';

                return array (
                    'map' => $chosenMap,
                    'race' => $chosenRace,
                    'players' => $players,
                    'winrates' => $winratesDescending,
                    'form' => $form->createView(),
                    'team' => $team,
                );

            } else if(is_null($chosenMap) && !is_null($chosenRace)) {
                foreach($players as $player) {
                    $allOverall = count($gameRepo->findAllGamesVsRaceByPlayers($player, $chosenRace));
                    $wonOverall = count($gameRepo->findWonGamesVsRaceByPlayers($player, $chosenRace));

                    if($allOverall == 0) {
                        $overallWinrate = 45;
                    } else {
                        $overallWinrate = round(($wonOverall/$allOverall), 2) * 100;
                    }

                    $winrates[$player->getNickname()] = $overallWinrate;
                    $players[$player->getNickname()] = $player;
                }

                $winratesDescending = $winrates;
                arsort($winratesDescending);

                if(count($winratesDescending) > 5) {
                    $difference = count($winratesDescending) - 5;
                    for($i = 0; $i < $difference; $i++) {
                        array_pop($winratesDescending);
                    }
                }

                $chosenMap = 'Unknown';

                return array (
                    'map' => $chosenMap,
                    'race' => $chosenRace,
                    'players' => $players,
                    'winrates' => $winratesDescending,
                    'form' => $form->createView(),
                    'team' => $team,
                );

            } else if(!is_null($chosenMap) && is_null($chosenRace)) {
                foreach($players as $player) {
                    $allOverall = count($gameRepo->findAllGamesByPlayersOnMap($chosenMap, $player));
                    $wonOverall = count($gameRepo->findWonGamesByPlayersOnMap($chosenMap, $player));

                    if($allOverall == 0) {
                        $overallWinrate = 45;
                    } else {
                        $overallWinrate = round(($wonOverall/$allOverall), 2) * 100;
                    }

                    $winrates[$player->getNickname()] = $overallWinrate;
                    $players[$player->getNickname()] = $player;
                }

                $winratesDescending = $winrates;
                arsort($winratesDescending);

                if(count($winratesDescending) > 5) {
                    $difference = count($winratesDescending) - 5;
                    for($i = 0; $i < $difference; $i++) {
                        array_pop($winratesDescending);
                    }
                }

                $chosenRace = 'Unknown';

                return array (
                    'map' => $chosenMap,
                    'race' => $chosenRace,
                    'players' => $players,
                    'winrates' => $winratesDescending,
                    'form' => $form->createView(),
                    'team' => $team,
                );
            } else {
                foreach($players as $player) {
                    $allOverall = count($gameRepo->findAllPlayersVsRaceGamesOnMap($chosenMap, $player, $chosenRace));
                    $wonOverall = count($gameRepo->findWonPlayersVsRaceGamesOnMap($chosenMap, $player, $chosenRace));

                    if($allOverall == 0) {
                        $overallWinrate = 45;
                    } else {
                        $overallWinrate = round(($wonOverall/$allOverall), 2) * 100;
                    }

                    $winrates[$player->getNickname()] = $overallWinrate;
                    $players[$player->getNickname()] = $player;
                }

                $winratesDescending = $winrates;
                arsort($winratesDescending);

                if(count($winratesDescending) > 5) {
                    $difference = count($winratesDescending) - 5;
                    for($i = 0; $i < $difference; $i++) {
                        array_pop($winratesDescending);
                    }
                }

                return array (
                    'map' => $chosenMap,
                    'race' => $chosenRace,
                    'players' => $players,
                    'winrates' => $winratesDescending,
                    'form' => $form->createView(),
                    'team' => $team,
                );
            }
        }

        return array (
            'form' => $form->createView(),
            'team' => $team,
        );
    }

    public function getMapStatistics($mapSlug)
    {
        $em = $this->getDoctrine()->getManager();
        $mapRepo = $em->getRepository('AppBundle:Map');
        $gameRepo = $em->getRepository('AppBundle:Game');
        $playerRepo = $em->getRepository('AppBundle:Player');

        $map = $mapRepo->findMapBySlug($mapSlug);

        $terranPlayers = $playerRepo->findAllPlayersByRace("Terran");
        $zergPlayers = $playerRepo->findAllPlayersByRace("Zerg");
        $protossPlayers = $playerRepo->findAllPlayersByRace("Protoss");

        $games = $gameRepo->findAllGamesByMap($map);
        $gamesTotal = count($games) /2;

        $wonTerranOverall = count($gameRepo->findWonRacePlayersGamesOnMap($map, $terranPlayers, "Terran"));
        $allTerranOverall = count($gameRepo->findAllRacePlayersGamesOnMap($map, $terranPlayers, "Terran"));

        if($allTerranOverall == 0) {
            $percentTerranOverall = '-';
        } else {
            $percentTerranOverall = round(($wonTerranOverall/$allTerranOverall), 2);
            $percentTerranOverall = ($percentTerranOverall * 100) . '%';
        }

        $wonZergOverall = count($gameRepo->findWonRacePlayersGamesOnMap($map, $zergPlayers, "Zerg"));
        $allZergOverall = count($gameRepo->findAllRacePlayersGamesOnMap($map, $zergPlayers, "Zerg"));

        if($allZergOverall == 0) {
            $percentZergOverall = '-';
        } else {
            $percentZergOverall = round(($wonZergOverall/$allZergOverall), 2);
            $percentZergOverall = ($percentZergOverall * 100) . '%';
        }

        $wonProtossOverall = count($gameRepo->findWonRacePlayersGamesOnMap($map, $protossPlayers, "Protoss"));
        $allProtossOverall = count($gameRepo->findAllRacePlayersGamesOnMap($map, $protossPlayers, "Protoss"));

        if($allProtossOverall == 0) {
            $percentProtossOverall = '-';
        } else {
            $percentProtossOverall = round(($wonProtossOverall/$allProtossOverall), 2);
            $percentProtossOverall = ($percentProtossOverall * 100) . '%';
        }

        $wonTvZ = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $terranPlayers, "Zerg"));
        $allTvZ = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $terranPlayers, "Zerg"));

        if($allTvZ == 0) {
            $percentTvZ = '-';
            $percentZvT = '-';
        } else {
            $percentTvZ = round(($wonTvZ/$allTvZ), 2);
            $percentZvT = 1-$percentTvZ;
            $percentTvZ = ($percentTvZ * 100 ) . '%';
            $percentZvT = ($percentZvT * 100 ) . '%';
        }

        $wonTvP = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $terranPlayers, "Protoss"));
        $allTvP = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $terranPlayers, "Protoss"));

        if($allTvP == 0) {
            $percentTvP = '-';
            $percentPvT = '-';
        } else {
            $percentTvP = round(($wonTvP/$allTvP), 2);
            $percentPvT = 1-$percentTvP;
            $percentTvP = ($percentTvP * 100 ) . '%';
            $percentPvT = ($percentPvT * 100 ) . '%';
        }

        $wonZvP = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $zergPlayers, "Protoss"));
        $allZvP = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $zergPlayers, "Protoss"));

        if($allZvP == 0) {
            $percentZvP = '-';
            $percentPvZ = '-';
        } else {
            $percentZvP = round(($wonZvP/$allZvP), 2);
            $percentPvZ = 1-$percentZvP;
            $percentZvP = ($percentZvP * 100 ) . '%';
            $percentPvZ = ($percentPvZ * 100 ) . '%';
        }

        return array(
            'map' => $map,
            'gamesTotal' => $gamesTotal,
            'percentTerran' => $percentTerranOverall,
            'percentZerg' => $percentZergOverall,
            'percentProtoss' => $percentProtossOverall,
            'percentTvZ' => $percentTvZ,
            'percentZvT' => $percentZvT,
            'percentTvP' => $percentTvP,
            'percentPvT' => $percentPvT,
            'percentZvP' => $percentZvP,
            'percentPvZ' => $percentPvZ,
        );
    }

    public function getRaceStatistics($race)
    {
        $em = $this->getDoctrine()->getManager();
        $mapRepo = $em->getRepository('AppBundle:Map');
        $gameRepo = $em->getRepository('AppBundle:Game');
        $playerRepo = $em->getRepository('AppBundle:Player');

        $racePlayers = $playerRepo->findAllPlayersByRace($race);
        $maps = $mapRepo->findAll();
        $mapWinrates = array();

        $allOverall = count($gameRepo->findAllRacePlayers($racePlayers, $race));
        $wonOverall = count($gameRepo->findWonRacePlayers($racePlayers, $race));

        if($allOverall == 0) {
            $overallWinrate = '-';
        } else {
            $overallWinrate = round(($wonOverall/$allOverall), 2);
            $overallWinrate = ($overallWinrate * 100) . '%';
        }

        $allTerran = count($gameRepo->findAllGamesVsRaceByPlayers($racePlayers, "Terran"));
        $wonTerran = count($gameRepo->findWonGamesVsRaceByPlayers($racePlayers, "Terran"));

        if($allTerran == 0) {
            $terranWinrate = '-';
        } else {
            $terranWinrate = round(($wonTerran/$allTerran), 2);
            $terranWinrate = ($terranWinrate * 100) . '%';
        }

        $allZerg = count($gameRepo->findAllGamesVsRaceByPlayers($racePlayers, "Zerg"));
        $wonZerg = count($gameRepo->findWonGamesVsRaceByPlayers($racePlayers, "Zerg"));

        if($allZerg == 0) {
            $zergWinrate = '-';
        } else {
            $zergWinrate = round(($wonZerg/$allZerg), 2);
            $zergWinrate = ($zergWinrate * 100) . '%';
        }

        $allProtoss = count($gameRepo->findAllGamesVsRaceByPlayers($racePlayers, "Protoss"));
        $wonProtoss = count($gameRepo->findWonGamesVsRaceByPlayers($racePlayers, "Protoss"));

        if($allProtoss == 0) {
            $protossWinrate = '-';
        } else {
            $protossWinrate = round(($wonProtoss/$allProtoss), 2);
            $protossWinrate = ($protossWinrate * 100) . '%';
        }

        switch($race) {
            case "Terran":
                $terranWinrate = '-'; break;
            case "Zerg":
                $zergWinrate = '-'; break;
            case "Protoss":
                $protossWinrate = '-'; break;
            default:
                $this->addFlash('warning',"Something went wrong!");
        }

        foreach($maps as $map) {
            $allOverall = count($gameRepo->findAllGamesByPlayersOnMap($map, $racePlayers));
            $wonOverall = count($gameRepo->findWonGamesByPlayersOnMap($map, $racePlayers));
            if($allOverall == 0) {
                $percentOverall = '-';
            } else {
                $percentOverall = round(($wonOverall/$allOverall), 2);
                $percentOverall = ($percentOverall * 100) . '%';
            }

            $wonTerran = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $racePlayers, "Terran"));
            $allTerran = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $racePlayers, "Terran"));
            if($allTerran == 0) {
                $percentTerran = '-';
            } else {
                $percentTerran = round(($wonTerran/$allTerran), 2);
                $percentTerran = ($percentTerran * 100) . '%';
            }

            $wonZerg = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $racePlayers, "Zerg"));
            $allZerg = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $racePlayers, "Zerg"));
            if($allZerg == 0) {
                $percentZerg = '-';
            } else {
                $percentZerg = round(($wonZerg/$allZerg), 2);
                $percentZerg = ($percentZerg * 100) . '%';
            }

            $wonProtoss = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $racePlayers, "Protoss"));
            $allProtoss = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $racePlayers, "Protoss"));
            if($allProtoss == 0) {
                $percentProtoss = '-';
            } else {
                $percentProtoss = round(($wonProtoss/$allProtoss), 2);
                $percentProtoss = ($percentProtoss * 100) . '%';
            }

            switch($race) {
                case "Terran":
                    $percentTerran = '-'; break;
                case "Zerg":
                    $percentZerg = '-'; break;
                case "Protoss":
                    $percentProtoss = '-'; break;
                default:
                    $this->addFlash('warning',"Something went wrong!");
            }

            $mapWinrates[$map->getMapName()]['Slug'] = $map->getSlug();
            $mapWinrates[$map->getMapName()]['Overall'] = $percentOverall;
            $mapWinrates[$map->getMapName()]['Terran'] = $percentTerran;
            $mapWinrates[$map->getMapName()]['Zerg'] = $percentZerg;
            $mapWinrates[$map->getMapName()]['Protoss'] = $percentProtoss;
        }

        return array(
            'overall' => $overallWinrate,
            'terran' => $terranWinrate,
            'zerg' => $zergWinrate,
            'protoss' => $protossWinrate,
            'mapWinrates' => $mapWinrates,
            'race' => $race,
        );
    }

    public function getPlayerStatistics($player)
    {
        $em = $this->getDoctrine()->getManager();
        $mapRepo = $em->getRepository('AppBundle:Map');
        $gameRepo = $em->getRepository('AppBundle:Game');

        $maps = $mapRepo->findAll(); // iterowanie po mapach po winratio
        $mapWinrates = array();

        $recentGames = $gameRepo->findRecentGamesByPlayers($player);
        $allGames = $gameRepo->findAllGamesByPlayers($player);

        $allOverall = count($gameRepo->findAllGamesByPlayers($player));
        $wonOverall = count($gameRepo->findWonGamesByPlayers($player));

        if($allOverall == 0) {
            $overallWinrate = '-';
        } else {
            $overallWinrate = round(($wonOverall/$allOverall), 2);
            $overallWinrate = ($overallWinrate * 100) . '%';
        }

        $allTerran = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Terran"));
        $wonTerran = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Terran"));

        if($allTerran == 0) {
            $terranWinrate = '-';
        } else {
            $terranWinrate = round(($wonTerran/$allTerran), 2);
            $terranWinrate = ($terranWinrate * 100) . '%';
        }

        $allZerg = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Zerg"));
        $wonZerg = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Zerg"));

        if($allZerg == 0) {
            $zergWinrate = '-';
        } else {
            $zergWinrate = round(($wonZerg/$allZerg), 2);
            $zergWinrate = ($zergWinrate * 100) . '%';
        }

        $allProtoss = count($gameRepo->findAllGamesVsRaceByPlayers($player, "Protoss"));
        $wonProtoss = count($gameRepo->findWonGamesVsRaceByPlayers($player, "Protoss"));

        if($allProtoss == 0) {
            $protossWinrate = '-';
        } else {
            $protossWinrate = round(($wonProtoss/$allProtoss), 2);
            $protossWinrate = ($protossWinrate * 100) . '%';
        }

        foreach($maps as $map) {
            $allOverall = count($gameRepo->findAllGamesByPlayersOnMap($map, $player));
            $wonOverall = count($gameRepo->findWonGamesByPlayersOnMap($map, $player));
            if($allOverall == 0) {
                $percentOverall = '-';
            } else {
                $percentOverall = round(($wonOverall/$allOverall), 2);
                $percentOverall = ($percentOverall * 100) . '%';
            }

            $wonTerran = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $player, "Terran"));
            $allTerran = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $player, "Terran"));
            if($allTerran == 0) {
                $percentTerran = '-';
            } else {
                $percentTerran = round(($wonTerran/$allTerran), 2);
                $percentTerran = ($percentTerran * 100) . '%';
            }

            $wonZerg = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $player, "Zerg"));
            $allZerg = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $player, "Zerg"));
            if($allZerg == 0) {
                $percentZerg = '-';
            } else {
                $percentZerg = round(($wonZerg/$allZerg), 2);
                $percentZerg = ($percentZerg * 100) . '%';
            }

            $wonProtoss = count($gameRepo->findWonPlayersVsRaceGamesOnMap($map, $player, "Protoss"));
            $allProtoss = count($gameRepo->findAllPlayersVsRaceGamesOnMap($map, $player, "Protoss"));
            if($allProtoss == 0) {
                $percentProtoss = '-';
            } else {
                $percentProtoss = round(($wonProtoss/$allProtoss), 2);
                $percentProtoss = ($percentProtoss * 100) . '%';
            }

            $mapWinrates[$map->getMapName()]['Slug'] = $map->getSlug();
            $mapWinrates[$map->getMapName()]['Overall'] = $percentOverall;
            $mapWinrates[$map->getMapName()]['Terran'] = $percentTerran;
            $mapWinrates[$map->getMapName()]['Zerg'] = $percentZerg;
            $mapWinrates[$map->getMapName()]['Protoss'] = $percentProtoss;
        }

        return array(
            'player' => $player,
            'allGames' => $allGames,
            'recentGames' => $recentGames,
            'overallWinrate' => $overallWinrate,
            'terranWinrate' => $terranWinrate,
            'zergWinrate' => $zergWinrate,
            'protossWinrate' => $protossWinrate,
            'mapWinrates' => $mapWinrates,
        );
    }
}
