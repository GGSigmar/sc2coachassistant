<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    public function getSecurityContext()
    {
        return $this->get('security.authorization_checker');
    }

    /**
     * @return bool
     */
    public function isUserLoggedIn() {
        if($this->getSecurityContext()->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enforceUserLoggedIn(Request $request) {
        if(!$this->isUserLoggedIn()) {
            $this->addFlash('warning', 'You have to be logged-in in order to access this, sir!');
            return $this->redirectToIndex();
        } else {
            return null;
        }
    }

    /**
     * @return bool
     */
    public function userHasTeam()
    {
        if($this->isUserLoggedIn()) {
            $user = $this->getUser();
            if($user->getTeam() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    /**
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enforceUserHasTeam(Request $request) {
        if(!$this->userHasTeam()) {
            $this->addFlash('warning', 'You have to have a team in order to access this, sir!');
            return $this->redirectToIndex();
        } else {
            return null;
        }
    }

    /**
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enforceAntiUserHasTeam(Request $request) {
        if($this->userHasTeam()) {
            $this->addFlash('warning', 'You already have a team, you sneaky you, sir!');
            return $this->redirectToIndex();
        } else {
            return null;
        }
    }

    /**
     * @return Team|string
     */
    public function getUserTeam()
    {
        if($this->userHasTeam()) {
            $user = $this->getUser();
            return $user->getTeam();
        } else {
            return "Team not found.";
        }
    }

    /**
     * @param $slug
     * @return bool
     */
    public function isCorrectTeam($slug)
    {
        if($this->getUserTeam()->getSlug() == $slug) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Player $player
     * @return bool
     */
    public function isCorrectPlayer(Player $player)
    {
        if($player->getTeam() == $this->getUserTeam()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Game $game
     * @return bool
     */
    public function isCorrectGame(Game $game)
    {
        if($game->getYourPlayer()->getTeam() == $this->getUserTeam())
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToIndex()
    { 
        $url = $this->generateUrl('index');
        return $this->redirect($url);
    }


}
