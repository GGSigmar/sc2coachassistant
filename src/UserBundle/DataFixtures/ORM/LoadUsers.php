<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsers implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $SKTelecomT1 = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'SK Telecom T1'
            ));

        $AfreecaFreecs = $manager->getRepository('AppBundle:Team')
            ->findOneBy(array (
                'teamName' => 'Afreeca Freecs'
            ));

        $sigmarUser = new User();
        $sigmarUser->setUsername("Sigmar");
        $sigmarUser->setEmail("sigmar@fixture.pl");
        $sigmarUser->setPlainPassword("sigmar");
        $sigmarUser->setRoles(array('ROLE_ADMIN'));
        $sigmarUser->setEnabled(true);

        $SKTmanager = $userManager->createUser();
        $SKTmanager->setUsername('iloveoov');
        $SKTmanager->setEmail('iloveoov@example.com');
        $SKTmanager->setPlainPassword('iloveoov');
        $SKTmanager->setTeam($SKTelecomT1);
        $SKTmanager->setEnabled(true);

        $AFmanager = $userManager->createUser();
        $AFmanager->setUsername('Legend');
        $AFmanager->setEmail('legend@example.com');
        $AFmanager->setPlainPassword('legend');
        $AFmanager->setTeam($AfreecaFreecs);
        $AFmanager->setEnabled(true);

        $manager->persist($sigmarUser);
        $manager->persist($SKTmanager);
        $manager->persist($AFmanager);

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
