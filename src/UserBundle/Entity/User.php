<?php

namespace UserBundle\Entity;

use AppBundle\Entity\Team;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="sc2ca_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Team",
     *     inversedBy="managers"
     * )
     */
    protected $team;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }

    /**
     * Sets Team to null
     */
    public function unsetTeam()
    {
        $this->team = null;
    }
}
